import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'
import Button from '@material-ui/core/Button';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { v4 } from 'uuid'

class TopbarComponet extends Component {
  constructor() {
    super();

    this.state = {

    }

  }

  componentDidMount() {}

  render() {

    return (
      <AppBar position="static">
        <Toolbar>
          <img onClick={ () => window.location.hash = "search" } style={ { height: 50 } } src={ `./images/symetryLogo.png` } />
          <div style={ { width: 20 } }></div>
          <Button onClick={ () => window.location.hash = "settings" } variant="contained" color="primary">
            Settings
          </Button>
          <div className="flex-1"></div>
          <Button onClick={ () => window.location.hash = "login?logout=true" } variant="contained" color="primary">
            logOut
          </Button>
        </Toolbar>
      </AppBar>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
})

const Topbar = connect(mapStateToProps, {

})(TopbarComponet)

export default Topbar