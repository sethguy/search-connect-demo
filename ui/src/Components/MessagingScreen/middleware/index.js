import { getConversation, sendMessage } from './firebaseService'

const messagingMiddleware = store => next => action => {

  switch (action.type) {
    case "getConversation": {
      var {conversationUid, accessToken} = action
      //need a check acces token function
      next(action)

      return getConversation(conversationUid, next, accessToken)
    }
    case "sendMessage": {
      var {conversationUid, messageData} = action

      next(action)

      return sendMessage(conversationUid, messageData, next)
    }
    default:
      return next(action)
  }

}

export default messagingMiddleware;