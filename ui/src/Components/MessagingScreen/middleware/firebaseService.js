import firebase from '../../../firebase'

var db = firebase.firestore();

export const getConversation = (conversationUid, next, accessToken) => {

  if (accessToken) {

    firebase.auth().signInWithCustomToken(accessToken)

      .then((authResponse) => {

        var {user: {uid, email}} = authResponse;

        next({
          type: "messageTokenAuthResponse",
          user: {
            uid,
            email
          }
        })

        return initConversation(next, conversationUid)

      })

      .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
      });

  } else {

    return initConversation(next, conversationUid)

  }

}

const initConversation = (next, conversationUid) => {

  var conversationRef = db.collection("conversations").doc(conversationUid);

  return conversationRef

    .get()

    .then((response) => {
      console.log('conversationResponse', response)

      var {docs} = response

      var doc = response
      return watchForConversationUpdates(doc.id, next)

        .then((data) => {

          var conversation = doc.data();

          conversation.id = doc.id;

          return next({
            type: "getConversationResponse",
            conversation
          })

        })
    })

    .catch(function(error) {

      return next({
        type: "getConversationError",
        error
      })

    });

}

export const sendMessage = (conversationUid, messageData, next) => {

  var conversationRef = db.collection("conversations").doc(conversationUid);

  conversationRef.get()

    .then(function(conversationResponse) {
      var {docs} = conversationResponse;

      if (conversationResponse.exists) {

        var doc = conversationResponse
        var conversation = doc.data();

        conversation.id = doc.id;

        return conversation
      }

    })

    .then((conversation) => {

      return conversationRef.update({
        messages: [...conversation.messages, messageData]
      })

    })

    .catch(function(error) {

      return next({
        type: "conversationError",
        error
      })

    });

}

export const watchForConversationUpdates = (conversationUid, next) => {

  return new Promise(function(resolve, reject) {

    db.collection("conversations").doc(conversationUid)
      .onSnapshot(function(doc) {
        resolve(next({
          type: "conversationUpdate",
          conversation: {
            id: doc.id,
            ...doc.data()
          }
        }))
      });

  });

}
