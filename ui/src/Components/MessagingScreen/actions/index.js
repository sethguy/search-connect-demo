export const getConversation = (conversationUid, accessToken) => (dispatch, getState) => {

  return dispatch({
    type: "getConversation",
    conversationUid,
    accessToken
  })

}

export const sendMessage = (conversationUid, messageData) => (dispatch, getState) => {

  return dispatch({
    type: "sendMessage",
    conversationUid,
    messageData
  })

}