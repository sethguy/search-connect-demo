import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'
import Topbar from '../Topbar/'

import { getConversation, sendMessage } from './actions';

import { v4 } from 'uuid'
import './messaging.css'

const messageFlexPosision = (userId, messageData) => {

  var author = messageData.from;

  return (userId === author) ? 'justify-content-end bg-me' : 'justify-content-start bg-you'

}

class MessagingScreenComponet extends Component {
  constructor() {
    super();

    this.state = {
      draft: ""
    }
  }

  pressEnter = (event) => {
    var {getConversation, sendMessage, locationUtil: {params: {conversationUid, accessToken}}} = this.props;

    if (event.keyCode == 13) {
      const {user, sendMessage} = this.props;
      const {draft} = this.state;

      sendMessage(conversationUid,
        {
          from: user.uid,
          text: draft,
          created: new Date().getTime()
        })

      this.setState({
        draft: ""
      })

    }

  }

  onInput = (event, value) => {

    this.setState({
      draft: value
    })

  }

  componentDidMount() {

    var {getConversation, locationUtil: {params: {conversationUid, accessToken}}} = this.props;
    if (conversationUid) {
      getConversation(conversationUid, accessToken)
    }

  }

  render() {

    var {messaginScreen: {messages}, user} = this.props;

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <div className="flex-1 scroll-y">
          { messages && messages
            
              .map((messageData) => (
                <div className={ `d-flex ${messageFlexPosision(user.uid,messageData)}` } key={ v4() }>
                  <span style={ { fontSize: '3vh' } } key={ v4() }>{ messageData.text }</span>
                </div>
              )
            
            ) }
        </div>
        <input value={ this.state.draft } onKeyUp={ (event) => this.pressEnter(event) } onChange={ (event) => this.onInput(event, event.target.value) } className="messaging-input" />
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  locationUtil: state.locationUtil,
  draft: state.draft,
  messaginScreen: state.messaginScreen
})

const MessagingScreen = connect(mapStateToProps, {
  getConversation,
  sendMessage
})(MessagingScreenComponet)

export default MessagingScreen