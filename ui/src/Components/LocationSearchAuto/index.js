import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import TextField from '@material-ui/core/TextField';
import { v4 } from 'uuid'

import PlacesAutocomplete from 'react-places-autocomplete';

import { geocodeByAddress, geocodeByPlaceId, getLatLng,
} from 'react-places-autocomplete';


class LocationSerachAuto extends Component {
  constructor() {
    super();

  }

  onChange = address => {

    this.props.onChange({
      target: {
        value: address
      }
    })
  };

  handleSelect = address => {

    geocodeByAddress(address)

      .then(results => getLatLng(results[0]))

      .then(latLng => {

        console.log('Success', latLng)
        this.props.onSelection({
          address,
          latLng
        })

      })
      .catch(error => console.error('Error', error));


  };

  render() {

    var {address} = this.props

    return (
      <PlacesAutocomplete value={ address } onChange={ this.onChange } margin="normal" onSelect={ this.handleSelect }>
        { ({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
            <div>
              <TextField {...getInputProps({ })} placeholder="Location" fullWidth InputLabelProps={ { shrink: true, } } />
              <div className="autocomplete-dropdown-container">
                { loading && <div>
                               Loading...
                             </div> }
                { suggestions.map(suggestion => {
                    const className = suggestion.active
                      ? 'suggestion-item--active'
                      : 'suggestion-item';
                    // inline style for demonstration purpose
                    const style = suggestion.active
                      ? {
                        backgroundColor: '#fafafa',
                        cursor: 'pointer'
                      }
                      : {
                        backgroundColor: '#ffffff',
                        cursor: 'pointer'
                      };
                    return (
                      <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                        <span>{ suggestion.description }</span>
                      </div>
                      );
                  }) }
              </div>
            </div>
          ) }
      </PlacesAutocomplete>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  locationUtil: state.locationUtil
})


export default connect(mapStateToProps, {

})(LocationSerachAuto)
