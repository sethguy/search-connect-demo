export const getUserProfileData = (uid) => (dispatch, getState) => {

  return dispatch({
    type: "getUserProfileData",
    uid
  })

}


export const sendMessageRequest = (toUid, toEmail) => (dispatch, getState) => {

  return dispatch({
    type: "sendMessageRequest",
    toUid,
    toEmail,
  })

}


export const getSearchTermsForUserProfile = (uid) => (dispatch, getState) => {

  return dispatch({
    type: "getSearchTermsForUserProfile",
    uid
  })

}