const userProfileReducer = (state = {}, action) => {

  switch (action.type) {

    case "userProfileResponse": {

      return {
        ...state,
        ...action.userProfile
      }

    }
    case "userSearchTermsForUserProfile": {

      var {userSearchTerms} = action;

      return {
        ...state,
        searchTerms: userSearchTerms.list
      }

    }
    default:
      return state;
  }

}

export default userProfileReducer;