import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'
import Topbar from '../Topbar/'

import { v4 } from 'uuid'

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';

import { getUserProfileData, sendMessageRequest, getSearchTermsForUserProfile } from './actions';

class UserProfileComponet extends Component {
  constructor() {
    super();
  }

  componentDidMount() {

    var {getUserProfileData, getSearchTermsForUserProfile, locationUtil: {params: {uid}}} = this.props;

    if (uid) {
      getUserProfileData(uid)

      getSearchTermsForUserProfile(uid)
    }
  }

  render() {

    var {userProfile} = {
      userProfile: {},
      ...this.props,
    }

    var profileSearchTerms = userProfile.searchTerms

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <br/>
        <br/>
        <br/>
        <div className="flex-1  scroll-y">
          <Card>
            <div title="Contemplative Reptile" className="d-flex justify-content-center" style={ { backgroundColor: 'black', height: 300, } }>
              <img className="h-100" style={ { width: 400 } } src={ userProfile.imageUrl } />
            </div>
            <CardContent>
              <div className="d-flex w-100">
                <Typography gutterBottom variant="headline" component="h2">
                  { userProfile.firstname }
                </Typography>
                <div className="m-1" />
                <Typography gutterBottom variant="headline" component="h2">
                  { userProfile.lastname }
                </Typography>
              </div>
              <div className="d-flex w-100">
                <Typography gutterBottom variant="headline" component="h2">
                  { userProfile.location }
                </Typography>
                <div className="m-1" />
              </div>
              { profileSearchTerms && profileSearchTerms.map(term => (<Chip key={ v4() } label={ term } />)) }
            </CardContent>
            <CardActions>
              <Button onClick={ (event) => this.props.sendMessageRequest(userProfile.userId, userProfile.email) } size="small" color="primary">
                Message
              </Button>
            </CardActions>
          </Card>
        </div>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  userProfile: state.userProfile,
  locationUtil: state.locationUtil
})

const UserProfile = connect(mapStateToProps, {
  getUserProfileData,
  sendMessageRequest,
  getSearchTermsForUserProfile
})(UserProfileComponet)

export default UserProfile