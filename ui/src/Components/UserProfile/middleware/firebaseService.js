import firebase from '../../../firebase'

var db = firebase.firestore();

export const getUserProfileData = (uid, next) => {

  var userProfileCollectionRef = db.collection("userProfiles");

  return userProfileCollectionRef.where("userId", "==", uid)

    .get()

    .then((response) => {

      var {docs} = response

      if (docs && docs.length) {

        var doc = docs[0]
        var userProfile = doc.data();

        userProfile.id = doc.id;
        return next({
          type: "userProfileResponse",
          userProfile
        })

      } else {

        return db.collection("userProfiles")

          .add({
            userId: uid
          })

          .then(function(userProfileResponse) {

            return next({
              type: "userProfileResponse",
              userProfile: {
                id: userProfileResponse.id
              }
            })

          })
      }

    })

    .catch(function(error) {

      return next({
        type: "userProfileError",
        error
      })

    });

}


export const getSearchTermsForUserProfile = (uid, next) => {

  return db.collection("userSearchTerms")

    .where('userId', '==', uid)
    .get()

    .then((response) => {

      var {docs} = response

      if (docs && docs.length) {

        var doc = docs[0]
        var userSearchTerms = {
          ...doc.data(),
          id: doc.id
        }

        return next({
          type: "userSearchTermsForUserProfile",
          userSearchTerms
        })
      }

    })
}


export const sendMessageRequest = (toUid, currentUid, toEmail, username, next) => {

  var conversationCollectionRef = db.collection("conversations");

  return conversationCollectionRef

    .where(`user-${toUid}`, "==", true)

    .where(`user-${currentUid}`, "==", true)

    .get()


    .then((response) => {
      var {docs} = response

      if (docs && docs.length) {

        var doc = docs[0]
        return createConversationRequest({
          conversationUid: doc.id,
          toEmail,
          username: username || toEmail,
          uid: toUid
        })
          .then((data) => {

            return watchForConversationUpdates(doc.id, next)

          })
          .then((data) => {

            var doc = docs[0]
            next({
              type: "conversationResponse",
              conversation: {
                ...doc.data(),
                id: doc.id
              }
            })

            return window.location.href = "/#/messaging?conversationUid=" + doc.id

          })

      } else {

        return db.collection("conversations")

          .add({
            [`user-${toUid}`]: true,
            [`user-${currentUid}`]: true,
            messages: []
          })

          .then(function(conversation) {

            return createConversationRequest({
              conversationUid: conversation.id,
              toEmail,
              username: username || toEmail,
              uid: toUid
            })

              .then((data) => {

                return watchForConversationUpdates(conversation.id, next)

              })
              .then((data) => {

                return {
                  id: conversation.id,
                  conversation: {
                    [`user-${toUid}`]: true,
                    [`user-${currentUid}`]: true,
                    messages: []
                  }

                }

              })

          })



          .then(function(sendMessageResponse) {

            next({
              type: "conversationResponse",
              conversation: {
                [`user-${toUid}`]: true,
                [`user-${currentUid}`]: true,
                messages: []
              }

            })

            return window.location.href = "/#/messaging?conversationUid=" + sendMessageResponse.id

          })
      }

    })

    .catch(function(error) {

      return next({
        type: "conversationError",
        error
      })

    });

}

export const watchForConversationUpdates = (conversationUid, next) => {


  db.collection("conversations").doc(conversationUid)
    .onSnapshot(function(doc) {


      return next({
        type: "conversationUpdate",
        conversation: {
          ...doc.data()
        }
      })
    // ...
    });
  return Promise.resolve()


}

export const createConversationRequest = ({conversationUid, toEmail, username, uid}) => {
  var createConversationRequestUrl = `https://us-central1-search-connect-symmetry.cloudfunctions.net/sendSymmetryMessageNotification`

  return fetch(createConversationRequestUrl, {
    url: createConversationRequestUrl,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      conversationUid,
      toEmail,
      username,
      uid
    })
  })
    .then(response => response.json())

}

