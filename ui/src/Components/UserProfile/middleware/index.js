import { getUserProfileData, sendMessageRequest, getSearchTermsForUserProfile } from './firebaseService'

const userProfileMiddleware = store => next => action => {

  switch (action.type) {
    case "getUserProfileData": {
      var {uid} = action

      next(action)

      return getUserProfileData(uid, next)
    }
    case "sendMessageRequest": {
      var {toUid, toEmail} = action
      var {user: {userProfile}} = store.getState()
      var username = userProfile.username || userProfile.email
      next(action)

      return sendMessageRequest(toUid, userProfile.userId, toEmail, username, next)
    }
    case "getSearchTermsForUserProfile": {
      var {uid} = action

      next(action)

      return getSearchTermsForUserProfile(uid, next)
    }
    default:
      return next(action)
  }

}

export default userProfileMiddleware;