import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'
import Topbar from '../Topbar/'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';

import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import { v4 } from 'uuid'

import Button from '@material-ui/core/Button';

import { saveProfile, loadProfile, uploadProfileImage } from './actions';

import LocationSearchAuto from '../LocationSearchAuto'


class EditProfileComponet extends Component {
  constructor() {
    super();

    this.state = {
      userProfile: {
        email: "",
        firstname: "",
        lastname: "",
        location: ""
      }
    }
  }

  setUserProfile(profileData) {
    return new Promise((resolve, reject) => {

      const {email, firstname, lastname, location, userId, id, imageUrl, latLng} = profileData

      this.setState({
        userProfile: {
          email: email || "",
          firstname: firstname || "",
          lastname: lastname || "",
          location: location || "",
          imageUrl: imageUrl || "",
          latLng: latLng || {},
          userId,
          id
        }
      }, () => {

        resolve(profileData)

      })
    })
  }

  componentDidMount() {

    var {paths: [], params} = this.props.locationUtil

    this.setUserProfile(this.props.user.userProfile)

  }

  onInputChange(event, feildName) {

    this.setState({
      userProfile: {
        ...this.state.userProfile,
        [feildName]: event.target.value
      }
    })

  }

  saveProfile = () => {

    const {email, firstname, lastname, userId, id, location, latLng, imageUrl} = this.state.userProfile

    console.log('this.sate', this.state.userProfile)
    this.props.saveProfile({
      profileData: {
        email,
        firstname,
        lastname,

        userId,
        id,
        imageUrl,
        location,
        latLng
      }
    })

  }

  uploadProfileImage = () => {

    this.processImageFileData()

      .then(({fileData, fileName}) => {

        return this.props.uploadProfileImage({
          fileData,
          fileName
        })

      })

      .then(({downloadUrl}) => {

        return this.setUserProfile({
          ...this.state.userProfile,
          imageUrl: downloadUrl
        })

      })

      .then(userProfile => {
        this.clearPreviewImage()

        this.saveProfile()

      })

  }

  setPreviewImage = (imageFileData) => {

    var reader = new FileReader();

    reader.onload = (e) => {

      this.setState({
        imagePreview: e.target.result

      })
    }

    reader.readAsDataURL(imageFileData);

  }

  clearPreviewImage = (imageFileData) => {

    this.setState({
      imagePreview: null
    })

  }

  processImageFileData = () => {

    return new Promise((resolve, reject) => {
      const {email, firstname, lastname, userId, id, imageUrl} = this.state.userProfile

      var canvas = document.createElement("canvas");
      var context = canvas.getContext('2d');

      var imageElement = document.getElementById('edit-profile-preview-image-element')
      var height = 300;
      var width = 300;

      canvas.height = height;
      canvas.width = width;

      context.drawImage(imageElement, 0, 0, width, height);

      var canvasBlobData = canvas.toBlob((toBlobResult) => {

        return resolve({
          fileData: toBlobResult,
          fileName: `${userId}-profile-image`
        })



      })
    })
  }

  onUploadInputChange = (event) => {

    var {files: [file]} = event.target

    this.setPreviewImage(file);

  }
  onLocationSelection(event) {


    this.setState({
      userProfile: {
        ...this.state.userProfile,
        location: event.address,
        latLng: event.latLng
      }
    })

  }
  render() {

    var {imageUrl, email, firstname, lastname, location} = this.state.userProfile;

    var {imagePreview} = this.state

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <div className="flex-1 scroll-y">
          <Card style={ { margin: 20, } }>
            <div title="Contemplative Reptile" className="w-100 d-flex justify-content-center" style={ { backgroundColor: 'black', height: 300 } }>
              { !imagePreview && imageUrl && (imageUrl.length > 0) && <img className="h-100" src={ imageUrl } /> }
              { imagePreview && <img id="edit-profile-preview-image-element" className="h-100" src={ imagePreview } /> }
            </div>
            <CardContent>
              { imagePreview
                && <div className="d-flex">
                     <Button onClick={ (event) => this.uploadProfileImage() } variant="contained" color="default">
                       Upload Profile Image
                       <input onChange={ (event) => this.onUploadInputChange(event) } style={ { display: 'none', height: 0, width: 0 } } type="file" name="profile-image-upload-input" id="profile-image-upload-input" />
                     </Button>
                     <div className="m-1" />
                     <Button onClick={ (event) => this.clearPreviewImage() } variant="contained" color="secondary">
                       Cancel
                       <input onChange={ (event) => this.onUploadInputChange(event) } style={ { display: 'none', height: 0, width: 0 } } type="file" name="profile-image-upload-input" id="profile-image-upload-input" />
                     </Button>
                   </div> }
              <div className="m-2" />
              <Button htmlFor="profile-image-upload-input" component="label" style={ {  } } variant="contained" color="primary">
                Update Profile Image
                <input onChange={ (event) => this.onUploadInputChange(event) } style={ { display: 'none', height: 0, width: 0 } } type="file" name="profile-image-upload-input" id="profile-image-upload-input" />
              </Button>
              <br/>
              <br/>
              <Typography component="h1">
                Edit Profile
              </Typography>
              <TextField value={ email } InputLabelProps={ { shrink: true, } } placeholder="First Name" fullWidth margin="normal" />
              <TextField value={ firstname } onChange={ event => this.onInputChange(event, 'firstname') } InputLabelProps={ { shrink: true, } } placeholder="First Name" fullWidth margin="normal" />
              <TextField value={ lastname } onChange={ event => this.onInputChange(event, 'lastname') } InputLabelProps={ { shrink: true, } } placeholder="last Name" fullWidth margin="normal" />
              <LocationSearchAuto {...{ address: location }} onChange={ (event) => this.onInputChange(event, 'location') } onSelection={ (event) => this.onLocationSelection(event) } />
            </CardContent>
            <CardActions>
              <Button onClick={ (event) => this.saveProfile() } size="small" color="primary">
                Save
              </Button>
            </CardActions>
          </Card>
        </div>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  locationUtil: state.locationUtil
})

const EditProfile = connect(mapStateToProps, {
  loadProfile,
  saveProfile,
  uploadProfileImage
})(EditProfileComponet)

export default EditProfile