

import firebase from '../../../firebase'
import { fireUpload } from './photo-upload-fire'

var db = firebase.firestore();




export const saveProfile = ({profileData, userSearchTerms}) => (dispatch) => {
  console.log("profileData", profileData)
  dispatch({
    type: "saveProfile",
    profileData,
    userSearchTerms
  })

  return db.collection('userProfiles').doc(profileData.id)

    .update(profileData)

    .then((response) => {

      return dispatch({
        type: "updateProfileResponse",
        userProfile: profileData,
        response
      })

    })

    .catch((error) => {

      return dispatch({
        type: "updateProfileError",
        error,

      })

    })

}

export const loadProfile = ({userId}) => (dispatch) => {

  dispatch({
    type: "loadProfile",
    userId
  })

  return db.collection('userProfiles')

    .where("userId", "==", userId).get()


}



export const uploadProfileImage = ({fileData, fileName}) => (dispatch) => {

  dispatch({
    type: "uploadProfileImage",
    fileData,
    fileName
  })

  return fireUpload({
    fileData,
    fileName
  })

    .then(({downloadUrl}) => {
      return dispatch({
        type: "uploadProfileImageComplete",
        downloadUrl,
      })

    })

}