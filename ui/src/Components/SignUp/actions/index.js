export const emailSignUp = (email, password) => (dispatch, getState) => {

  return dispatch({
    type: "emailSignUp",
    signUp: {
      email,
      password
    }
  })

}

export const initFacebookSignUp = (email, password) => (dispatch, getState) => {

  return dispatch({
    type: "initFacebookSignUp",

  })

}