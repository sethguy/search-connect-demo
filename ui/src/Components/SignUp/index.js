import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'

import Button from '@material-ui/core/Button';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';


import firebase from '../../firebase'
import { emailSignUp, initFacebookSignUp } from './actions'

class SignUpComponet extends Component {
  constructor() {
    super();
    this.state = {

      email: "",
      password: ""
    }
  }

  signUpWithEmail = () => {

    var {email, password} = this.state
    this.props.emailSignUp(email, password)

  }

  signUpWithFaceBook = () => {

    this.props.initFacebookSignUp()

  }

  render() {

    return (
      <div className="h-100 d-flex  flex-column align-items-center">
        <br/>
        <br/>
        <img style={ { height: 100 } } src={ `./images/symetryLogo.png` } />
        <br/>
        <Typography gutterBottom variant="headline" component="h2">
          Sign Up
        </Typography>
        <br/>
        <Card>
          <CardContent>
            <TextField value={ this.state.email } onChange={ event => this.setState({
                                                               email: event.target.value
                                                             }) } InputLabelProps={ { shrink: true, } } placeholder="Email" fullWidth margin="normal" />
            <TextField value={ this.state.password } type="password" onChange={ event => this.setState({
                                                                                  password: event.target.value
                                                                                }) } InputLabelProps={ { shrink: true, } } placeholder="Password" fullWidth margin="normal"
            />
            <Button onClick={ () => this.signUpWithEmail() } variant="contained" color="primary">
              Sign Up
            </Button>
            <br/>
            <div className="d-flex flex-column align-items-center w-100">
              <hr className="w-100" /> or
              <hr className="w-100" />
              <Button onClick={ () => this.signUpWithFaceBook() } variant="contained" color="primary">
                Sign up with Facebook
              </Button>
              <br/>
              <Button onClick={ () => window.location.hash = "login" } color="primary">
                Login Screen
              </Button>
            </div>
          </CardContent>
        </Card>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user
})

const SignUp = connect(mapStateToProps, {
  emailSignUp,
  initFacebookSignUp
})(SignUpComponet)

export default SignUp