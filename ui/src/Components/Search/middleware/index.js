import { addSearchTerm, removeTerm, getMatches, getUserSearchTerms } from './firebaseSearchTermsService.js'
const searchchMiddleware = store => next => action => {

  switch (action.type) {
    case "addSearchTerm": {

      var {user: {email, uid}} = store.getState()

      var {newTerm} = action
      next(action)
      return addSearchTerm(newTerm, uid, next)

    }
    case "getMatches": {

      var {user: {email, uid}} = store.getState()

      next(action)

      return getMatches(uid, next)

    }
    case "removeTerm": {
      var {user: {email, uid}} = store.getState()

      var {removeIndex, termToRemove} = action
      next(action)
      return removeTerm(termToRemove, uid, next);

    }
    case "facebookSignUpResponse":
    case "userSignUpResponse":
    case "facebookSignInResponse":
    case "userSignInResponse": {
      next(action)
      return getUserSearchTerms(action.user, next)


    }
    default:
      return next(action)
  }

}

export default searchchMiddleware;