import firebase from '../../../firebase'

var db = firebase.firestore();

export const removeTerm = (termToRemove, userId, next) => {

  var userSearchTermsCollectionRef = db.collection("userSearchTerms");

  return userSearchTermsCollectionRef

    .where("userId", "==", userId).get()

    .then(function(userSearchTermsResponse) {

      var {docs} = userSearchTermsResponse;

      if (docs && docs.length > 0) {

        var doc = docs[0]
        var userSearchTerms = doc.data();

        userSearchTerms.id = doc.id;

        return userSearchTerms
      }

    })

    .then((userSearchTerms) => {

      var userSearchTermsRef = db.collection("userSearchTerms").doc(userSearchTerms.id);

      return userSearchTermsRef.update({
        list: [...userSearchTerms.list.filter(term => term != termToRemove)]
      })

    })

    .then(function(documentUpdateResponse) {
      console.log("Document successfully updated!", documentUpdateResponse);
    })
    .catch(function(error) {
      // The document probably doesn't exist.
      console.error("Error updating document: ", error);
    });

}


export const addSearchTerm = (term, userId, next) => {

  var newUserSearchTerms = {
    userId: userId,
    list: []

  }

  var userSearchTermsCollectionRef = db.collection("userSearchTerms");

  return userSearchTermsCollectionRef.where("userId", "==", userId)

    .get()

    .then(function(userSearchTermsResponse) {

      var {docs} = userSearchTermsResponse;

      if (docs && docs.length > 0) {


        var doc = docs[0]
        var userSearchTerms = doc.data();

        userSearchTerms.id = doc.id;

        return userSearchTerms
      } else {

        return db.collection("userSearchTerms").add(newUserSearchTerms)
          .then(function(newUserSearchTermsResponse) {
            return {
              ...newUserSearchTerms,
              id: newUserSearchTermsResponse.id
            }
          })

      }

    })

    .then((userSearchTerms) => {

      var userSearchTermsRef = db.collection("userSearchTerms").doc(userSearchTerms.id);

      return userSearchTermsRef.update({
        list: [...userSearchTerms.list, term]
      })

    })

    .then(function(documentUpdateResponse) {
      console.log("Document successfully updated!", documentUpdateResponse);
    })
    .catch(function(error) {
      // The document probably doesn't exist.
      console.error("Error updating document: ", error);
    });

}

export const getMatches = (userId, next) => {

  var userSearchTermsCollectionRef = db.collection("userSearchTerms");

  return userSearchTermsCollectionRef
    .get()

    .then(function(userSearchTermsResponse) {

      var {docs} = userSearchTermsResponse;

      var matches = docs.map(dataDoc => {

        return {
          id: dataDoc.id,
          ...dataDoc.data()
        }

      })

        .filter(match => match.userId != userId)

      getMathProfiles(matches, next)

      return next({
        type: "getMatchesResponse",
        matches
      })

    })

}

const getMathProfiles = (matches, next) => {

  matches.forEach(({userId}) => {

    var userProfileCollectionRes = db.collection('userProfiles')
    userProfileCollectionRes.where("userId", "==", userId).get()

      .then(({docs}) => {


        if (docs && docs.length > 0) {
          var userProfileDoc = docs[0]
          return next({
            type: "onMatchProfile",
            userProfile: {

              ...userProfileDoc.data(),
              id: userProfileDoc.id
            }
          })

        }

      })

  })

} //getMathProfiles

export const getUserSearchTerms = (user, next) => {
  var newUserSearchTerms = {
    userId: user.id,
    list: []

  }
  var {uid} = user;

  var userSearchTermsCollectionRef = db.collection("userSearchTerms");

  return userSearchTermsCollectionRef.where("userId", "==", uid)

    .get()

    .then(function(userSearchTermsResponse) {

      var {docs} = userSearchTermsResponse;

      if (docs && docs.length > 0) {


        var doc = docs[0]
        var userSearchTerms = doc.data();

        userSearchTerms.id = doc.id;

        return userSearchTerms
      } else {

        return db.collection("userSearchTerms").add(newUserSearchTerms)
          .then(function(newUserSearchTermsResponse) {
            return {
              ...newUserSearchTerms,
              id: newUserSearchTermsResponse.id
            }
          })

      }

    })

    .then((userSearchTerms) => {

      next({
        type: "setTerms",
        terms: userSearchTerms.list
      })

    })



}





