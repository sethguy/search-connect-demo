const searchReducer = (state = {}, action) => {
  switch (action.type) {

    case "addSearchTerm": {

      const terms = state.userSearchTerms || [];

      return {
        ...state,
        userSearchTerms: [...terms, action.newTerm]
      }
    }
    case "setTerms": {

      return {
        ...state,
        userSearchTerms: action.terms
      }

    }
    case "onMatchProfile": {

      var {userProfile} = action

      return {
        ...state,
        matches: state.matches.reduce((matches, match) => {

          if (userProfile.userId == match.userId) {

            return [...matches, {
              ...match,
              ...userProfile
            }]

          } else {

            return [...matches, match]

          }

        }, [])
      }
    }
    case "getMatchesResponse": {

      return {
        ...state,
        matches: action.matches
      }
    }
    case "completelogout": {

      return {
        ...state,
        userSearchTerms: [],

      }
    }
    case "removeTerm": {

      const terms = state.userSearchTerms || [];

      return {
        ...state,
        userSearchTerms: terms.reduce((userSearchTerms, term, i) => {

          if (i == action.removeIndex) {

            return userSearchTerms;

          }
          return [...userSearchTerms, term]


        }, [])
      }
    }
    default:
      return state;
  }

}

export default searchReducer;