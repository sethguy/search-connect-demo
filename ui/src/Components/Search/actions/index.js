export const addSearchTerm = (newTerm) => (dispatch, getState) => {

  return dispatch({
    type: "addSearchTerm",
    newTerm
  })

}

export const getMatches = () => (dispatch, getState) => {

  return dispatch({
    type: "getMatches",
  })

}

export const removeTerm = (removeIndex, termToRemove) => (dispatch, getState) => {

  return dispatch({
    type: "removeTerm",
    removeIndex,
    termToRemove
  })

}