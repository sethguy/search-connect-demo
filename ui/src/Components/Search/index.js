import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import { v4 } from 'uuid'

import { addSearchTerm, removeTerm, getMatches } from './actions'

import './search.css';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import { getUserProfileData, getSearchTermsForUserProfile } from '../UserProfile/actions';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Topbar from '../Topbar/'

class SearchComponet extends Component {
  constructor() {
    super();

    this.state = {
      searchValue: "",
      matches: []
    }

  }

  onSearchKeyUp = (event) => {
    if (event.keyCode == 13) {

      this.props.addSearchTerm(this.state.searchValue)

      this.getMatches()

      this.setState({
        searchValue: ""
      })

    }

  }

  removeTerm = (term, i) => {

    this.props.removeTerm(i, term)

  }

  onSearchInput = (event) => {

    this.setState({
      searchValue: event.target.value,
    })

  }

  getMatches = () => {

    this.props.getMatches()

  }
  componentDidMount() {

    this.getMatches();

  }

  render() {

    var {user, searchReducer: {matches, userSearchTerms}} = this.props

    if (!user || !user.uid) {

      window.location.hash = "login"
    }

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <br/>
        <Card>
          <CardContent>
            <TextField onKeyUp={ (event) => this.onSearchKeyUp(event) } onChange={ (event) => this.onSearchInput(event) } value={ this.state.searchValue } InputLabelProps={ { shrink: true, } } placeholder="Enter Search Term"
              fullWidth margin="normal" />
            <div>
              { userSearchTerms && userSearchTerms.map((term, i) => (
                  <Chip key={ v4() } label={ term } onDelete={ () => this.removeTerm(term, i) } />)
                ) }
            </div>
          </CardContent>
          <CardActions>
            <Button onClick={ (event) => {
                              
                                window.location.hash = "settings"
                              
                              } } size="small" color="primary">
              set filter settings
            </Button>
          </CardActions>
        </Card>
        <hr className="w-100" />
        <div className="flex-1 scroll-y d-flex flex-wrap justify-content-center">
          { matches && matches.map((match, i) => {
            
              return (<Card className={ `animated bounceInUp wait-${(i % 5)}` } key={ v4() } style={ { margin: 20, border: "1px solid black", height: 200, width: 200 } }>
                        <div title="Contemplative Reptile" style={ { backgroundColor: 'black', height: 100 } }>
                          <img className="h-100 w-100" src={ match.imageUrl } />
                        </div>
                        <CardContent>
                          <Typography component="p">
                            { match.username || match.email }
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button onClick={ (event) => {
                                            
                                              this.props.getUserProfileData(match.userId)
                                            
                                              this.props.getSearchTermsForUserProfile(match.userId)
                                            
                                              window.location.hash = "user-profile?uid=" + match.userId
                                            
                                            } } size="small" color="primary">
                            see Pofile
                          </Button>
                        </CardActions>
                      </Card> )
            
            }) }
        </div>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  searchReducer: state.searchReducer
})

const Search = connect(mapStateToProps, {
  addSearchTerm,
  removeTerm,
  getUserProfileData,
  getMatches,
  getSearchTermsForUserProfile
})(SearchComponet)

export default Search