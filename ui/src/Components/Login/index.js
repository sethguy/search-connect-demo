import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'

import Button from '@material-ui/core/Button';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import firebase from '../../firebase'

import { emailSignIn, initFacebookSignIn, completelogout } from './actions'

class LoginComponet extends Component {
  constructor() {
    super();
    this.state = {

      email: "",
      password: ""
    }
  }

  signInWithEmail = () => {

    var {email, password} = this.state

    this.props.emailSignIn(email, password)

  }

  signInWithFaceBook = () => {

    this.props.initFacebookSignIn()

  }

  componentDidMount() {

    if (window.location.hash == "#/login?logout=true") {

      this.props.completelogout()
      window.location.hash = "login"

    }

  }

  render() {

    var {user, locationUtil: {paths: [blank, first]}} = this.props

    if (user.uid && window.location.hash == "#/login") {

      window.location.hash = "/"

    }

    return (
      <div className="h-100 d-flex  flex-column align-items-center">
        <br/>
        <img style={ { height: 100 } } src={ `./images/symetryLogo.png` } />
        <br/>
        <Typography gutterBottom variant="headline" component="h2">
          Login
        </Typography>
        <br/>
        <Card>
          <CardContent>
            <TextField value={ this.state.email } onChange={ event => this.setState({
                                                               email: event.target.value
                                                             }) } InputLabelProps={ { shrink: true, } } placeholder="Email" fullWidth margin="normal" />
            <TextField value={ this.state.password } type="password" onChange={ event => this.setState({
                                                                                  password: event.target.value
                                                                                }) } InputLabelProps={ { shrink: true, } } placeholder="Password" fullWidth margin="normal"
            />
            <Button onClick={ () => this.signInWithEmail() } variant="contained" color="primary">
              Sign In
            </Button>
            <br/>
            <div className="d-flex flex-column align-items-center w-100">
              <hr className="w-100" /> or
              <hr className="w-100" />
              <Button onClick={ () => this.signInWithFaceBook() } variant="contained" color="primary">
                Sign in with Facebook
              </Button>
              <br/>
              <Button color="primary">
                Forgot Password ?
              </Button>
              <br/>
              <Button onClick={ () => window.location.hash = "sign-up" } color="primary">
                Sign Up Screen
              </Button>
            </div>
          </CardContent>
        </Card>
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  locationUtil: state.locationUtil
})

const Login = connect(mapStateToProps, {
  emailSignIn,
  initFacebookSignIn,
  completelogout,
})(LoginComponet)

export default Login