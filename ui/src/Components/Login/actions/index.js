
export const emailSignIn = (email, password) => (dispatch, getState) => {

  return dispatch({
    type: "emailSignIn",
    signIn: {
      email,
      password
    }
  })

}

export const completelogout = () => (dispatch, getState) => {

  return dispatch({
    type: "completelogout",

  })

}

export const initFacebookSignIn = (email, password) => (dispatch, getState) => {

  return dispatch({
    type: "initFacebookSignIn",

  })

}