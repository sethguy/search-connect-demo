import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { Route } from 'react-router-dom'

import Typography from '@material-ui/core/Typography';

import TextField from '@material-ui/core/TextField';
import { v4 } from 'uuid'

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import Topbar from '../Topbar/'

class SettingsComponent extends Component {
  constructor() {
    super();

    this.state = {
      serachRadius: "",
      location: "",
      searchRadius: 0,
      searchPreferance: {
        men: true,
        women: true
      }
    }

  }

  toggleSearchPreference(type) {

    var {men, women} = this.state.searchPreferance
    console.log(this.state.searchPreferance)
    this.setState({
      searchPreferance: {
        ...this.state.searchPreferance,
        [type]: !this.state.searchPreferance[type]
      }
    })

  }

  onInputChange = (event) => {

    this.setState({
      searchRadius: event.target.value
    })


  }

  componentDidMount() {}

  render() {

    var {user, searchReducer: {matches, userSearchTerms}} = this.props

    var {location, searchRadius, searchPreferance: {men, women}} = this.state

    return (
      <div className="h-100 d-flex flex-column">
        <Topbar/>
        <br/>
        <div className="flex-1 scroll-y">
          <Card>
            <CardContent>
              <Typography component="h1">
                Settings
              </Typography>
              <br/>
              <Button onClick={ (event) => {
                                  window.location.hash = "edit-profile"
                                } } variant="contained" color="primary">
                edit Profile
              </Button>
              <hr className="w-100" />
              <TextField value={ searchRadius } onChange={ event => this.onInputChange(event, 'searchRadius') } type="number" InputLabelProps={ { shrink: true, } } label="Search Radius In Miles" fullWidth margin="normal"
              />
              <br/>
              <p>
                Search Preferance
              </p>
              <div className="d-flex">
                { men && <Button onClick={ (event) => {
                                    
                                      this.toggleSearchPreference('men')
                                    } } variant="contained" color="primary">
                           men
                         </Button> }
                { !men && <Button onClick={ (event) => {
                                    
                                      this.toggleSearchPreference('men')
                                    } } variant="contained">
                            men
                          </Button> }
                <div style={ { width: 10 } } />
                { women && <Button onClick={ (event) => {
                                    
                                      this.toggleSearchPreference('women')
                                    } } variant="contained" color="primary">
                             women
                           </Button> }
                { !women && <Button onClick={ (event) => {
                                    
                                      this.toggleSearchPreference('women')
                                    } } variant="contained">
                              women
                            </Button> }
              </div>
            </CardContent>
            <CardActions>
              <Button onClick={ (event) => {
                                
                                
                                } } variant="contained" color="primary">
                save
              </Button>
            </CardActions>
          </Card>
        </div>
        <hr className="w-100" />
      </div>
      );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  searchReducer: state.searchReducer
})

const Search = connect(mapStateToProps, {

})(SettingsComponent)

export default Search