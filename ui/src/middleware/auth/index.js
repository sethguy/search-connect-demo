import { emailSignIn, emailSignUp, initFacebookSignIn, initFacebookSignUp } from './firebaseService'

const authMiddleware = store => next => action => {

  switch (action.type) {
    case "emailSignUp": {
      var {email, password} = action.signUp

      next(action)

      return emailSignUp(email, password, next).then((singUp) => {

        window.location.hash = "/edit-profile"

      })
    }
    case "emailSignIn": {
      var {email, password} = action.signIn

      next(action)

      return emailSignIn(email, password, next)
    }
    case "initFacebookSignUp": {

      next(action)
      return initFacebookSignUp(next)

    }
    case "initFacebookSignIn": {

      next(action)
      return initFacebookSignIn(next)

    }
    default:
      return next(action)
  }

}

export default authMiddleware;