import firebase from '../../firebase'
import { setUpUserProfile, getUserProfileData } from './userSignUpSetUpService'

var provider = new firebase.auth.FacebookAuthProvider();
provider.addScope('email');
provider.addScope('user_likes');


export const emailSignIn = (email, password, next) => {

  return firebase.auth().signInWithEmailAndPassword(email, password)


    .then((signInResponse) => {

      var {user: {uid, email}} = signInResponse
      var signInData = {
        uid,
        email
      }
      return getUserProfileData(signInData, next)

        .then(userProfile => {

          return signInResponse

        })
    })

    .then((signInResponse) => {

      var {user: {uid, email}} = signInResponse

      return next({
        type: "userSignInResponse",
        user: {
          uid,
          email
        }
      })

    })

    .catch(function(error) {

      return next({
        type: "userSignInError",
        error
      })

    });

}



export const emailSignUp = (email, password, next) => {

  return firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)

    .then((signUpResponse) => {

      var {user: {uid, email}} = signUpResponse
      var signUpData = {
        uid,
        email
      }
      return setUpUserProfile(signUpData, next)

        .then(userProfile => {

          return signUpResponse

        })
    })

    .then((signUpResponse) => {

      var {user: {uid, email}} = signUpResponse

      return next({
        type: "userSignUpResponse",
        user: {
          uid,
          email
        }
      })



    })

    .catch(function(error) {

      return next({
        type: "userSignUpError",
        error
      })

    });

}



export const initFacebookSignIn = (next) => {

  return firebase.auth().signInWithPopup(provider)

    .then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;

      return next({
        type: "facebookSignInResponse",
        user: user,
        token: token
      })

    })
    .catch(function(error) {

      return next({
        type: "facebookSignInError",
        error
      })

    });

}


export const initFacebookSignUp = (next) => {

  return firebase.auth().signInWithPopup(provider)

    .then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;

      return next({
        type: "facebookSignUpResponse",
        user: user,
        token: token
      })

    })
    .catch(function(error) {

      return next({
        type: "facebookSignUpError",
        error
      })

    });

}