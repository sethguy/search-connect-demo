import firebase from '../../firebase'

var db = firebase.firestore();

export const setUpUserProfile = (userSignUpData, next) => {

  var {email, username, profilepic, uid} = userSignUpData

  return db.collection("userProfiles")

    .add({
      email: email || "",
      username: username || "",
      profilepic: profilepic || "",
      userId: uid || ""
    })

    .then((userProfileResponse) => {

      return next({
        type: "userProfileCreated",
        userProfile: {
          id: userProfileResponse.id,
          ...{
            email: email || "",
            username: username || "",
            profilepic: profilepic || "",
            userId: uid || ""
          }
        }
      })

    })

    .catch(function(error) {

      return next({
        type: "userProfileError",
        error
      })

    });

}



export const getUserProfileData = (userSignInData, next) => {


  var {uid, email} = userSignInData

  var userProfileCollectionRef = db.collection("userProfiles");

  return userProfileCollectionRef.where("userId", "==", uid)

    .get()

    .then((response) => {

      var {docs} = response

      if (docs && docs.length) {

        var doc = docs[0]
        var userProfile = doc.data();

        userProfile.id = doc.id;
        return next({
          type: "loginUserProfileResponse",
          userProfile
        })

      } else {

        return db.collection("userProfiles")

          .add({
            userId: uid,
            email
          })

          .then(function(userProfileResponse) {

            return next({
              type: "userProfileResponse",
              userProfile: {
                id: userProfileResponse.id
              }
            })

          })
      }

    })

    .catch(function(error) {

      return next({
        type: "userProfileError",
        error
      })

    });

}