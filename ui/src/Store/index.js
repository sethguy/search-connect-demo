import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'

import { rootReducer } from '../reducers/'

import { httpRequest, authMiddleware, searchchMiddleware, userProfileMiddleware, messagingMiddleware } from '../middleware';

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk, httpRequest, authMiddleware, searchchMiddleware, userProfileMiddleware, messagingMiddleware, createLogger()),
    )
  )

  return store
}

var preloadedState = {
  user: {

  }
}
var searchConnectsymmetryStateKey = 'searchConnectsymmetry.preloadedState';

var StoredState = localStorage.getItem(searchConnectsymmetryStateKey) || JSON.stringify(preloadedState);

preloadedState = JSON.parse(StoredState)

console.log(' preloadedState ', preloadedState)

preloadedState = {
  ...preloadedState
}

export const store = configureStore(preloadedState);

store.subscribe(() => {

  var state = store.getState();

  localStorage.setItem(searchConnectsymmetryStateKey, JSON.stringify({
    ...state,
    searchReducer: {
      ...state.searchReducer,
      matches: []
    }

  }))

})