import firebase from "firebase";

// https://firebase.google.com/docs/web/setup?authuser=0

// See firebase setup in above google firebase documentation url
export const config = {
  apiKey: "AIzaSyAe7hWNGWXu0-17VcuEz4HrH1zHE3PCv-I",
  authDomain: "search-connect-symmetry.firebaseapp.com",
  databaseURL: "https://search-connect-symmetry.firebaseio.com",
  projectId: "search-connect-symmetry",
  storageBucket: "search-connect-symmetry.appspot.com",
  messagingSenderId: "573407990515"
};

firebase.initializeApp(config);

const firestore = firebase.firestore();
const settings = { /* your settings... */
  timestampsInSnapshots: true
};
firestore.settings(settings);
export default firebase;