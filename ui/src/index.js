import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux'

import { store } from './Store';

import { HashRouter } from 'react-router-dom'

import createHistory from 'history/createBrowserHistory'

import Home from './Components/Home/';
import Search from './Components/Search/';
import Login from './Components/Login/';
import SignUp from './Components/SignUp/';

import MessagingScreen from './Components/MessagingScreen/';
import UserProfile from './Components/UserProfile/';
import EditProfile from './Components/EditProfile/';
import SettingsComponent from './Components/Settings/';



import { Route } from 'react-router-dom'

const Root = ({store, history}) => (
  <Provider store={ store }>
    <HashRouter history={ history } basename='/'>
      <div className="h-100">
        <Route exact path="/" component={ Search } />
        <Route exact path="/search" component={ Search } />
        <Route exact path="/logout" component={ Login } />
        <Route exact path="/login" component={ Login } />
        <Route exact path="/sign-up" component={ SignUp } />
        <Route exact path="/messaging" component={ MessagingScreen } />
        <Route exact path="/user-profile" component={ UserProfile } />
        <Route exact path="/edit-profile" component={ EditProfile } />
        <Route exact path="/settings" component={ SettingsComponent } />
      </div>
    </HashRouter>
  </Provider>
)

render(<Root store={ store } />, document.getElementById('root'));