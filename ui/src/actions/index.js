const ACTION_TYPE = "ACTION"

export const ActionTypes = {
  ACTION_TYPE
}


const ActionCreator = (data) => {

  return {
    type: ACTION_TYPE
  }

}

export const actionDispachConnector = (data) => (dispatch, getState) => {

  return dispatch(ActionCreator(data))
}
