import { combineReducers } from 'redux'


import searchReducer from '../Components/Search/reducer'
import locationUtil from './location'

import userProfile from '../Components/UserProfile/reducer'


import messaginScreen from '../Components/MessagingScreen/reducer'
const user = (state = {}, action) => {
  switch (action.type) {
    case "messageTokenAuthResponse":

    case "facebookSignUpResponse":
    case "userSignUpResponse":
    case "facebookSignInResponse":
    case "userSignInResponse": {

      return {
        ...state,
        ...action.user
      }
    }
    case "userProfileCreated":
    case "updateProfileResponse":
    case "loginUserProfileResponse": {

      return {
        ...state,
        userProfile: action.userProfile

      }
    }
    case "completelogout": {

      return {

      }
    }
    default:
      return state;
  }

}

export const rootReducer = combineReducers({
  user,
  searchReducer,
  locationUtil,
  userProfile,
  messaginScreen
})