const cors = require("cors")();
const notifications = require("./notifications");

exports.sendSymmetryMessageNotification = (req, res) => {
  // get user token from request
  cors(req, res, () => {

    var {conversationUid, toEmail, username, uid} = req.body

    notifications.sendEmail({
      conversationUid,
      toEmail,
      username,
      uid
    })
      .then((sendEmailResponse) => {

        return res.status(200).send(sendEmailResponse);

      })
      .catch(err => {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
      });
  }); //cores
};