var admin = require("firebase-admin");
admin.initializeApp();

var createAdminToken = (uid) => {

  return admin.auth().createCustomToken(uid)

}

module.exports = {
  createAdminToken
}