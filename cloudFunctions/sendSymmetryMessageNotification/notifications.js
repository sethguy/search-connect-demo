const sgMail = require('@sendgrid/mail');
var symmetrybaseUrl = "https://search-connect-symmetry.firebaseapp.com";


var firebaseService = require('./firebaseService')

var sendEmail = (info) => {

  var {conversationUid, toEmail, username, uid} = info;
  console.log("uiduid 1", uid)
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  return firebaseService.createAdminToken(uid)

    .then(userSignInToken => {
      var userProfile = symmetrybaseUrl + "/#/user-profile?uid=" + uid

      var chatConvoLink = symmetrybaseUrl + `/#/messaging?conversationUid=${conversationUid}&accessToken=${userSignInToken}`

      var htmlMsg = `<p> ${username} wolud like to connect </p>

      <a href="${userProfile}"> click here to their view profile </a>

      <br/>
      <br/>
      <a href="${chatConvoLink}"> click here to  start conversation </a>`

      const msg = {
        from: 'alerts@symmetryconnect.ninja',
        to: toEmail, // list of receivers
        subject: '!?! SYMMETRY $ CHAT $ ALERT !?!', // Subject line
        html: htmlMsg
      };

      return sgMail.send(msg);

    })

}

module.exports = {

  sendEmail
}